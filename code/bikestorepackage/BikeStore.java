package bikestorepackage;

import bicyclepackage.Bicycle;

public class BikeStore{
  public static void main(String[] args){

    Bicycle[] bikes= new Bicycle[4];

    bikes[0]= new Bicycle("Trek", 12, 30.4);
    bikes[1]= new Bicycle("Giant", 15, 35);
    bikes[2]= new Bicycle("Specialized", 16, 25.5);
    bikes[3]= new Bicycle("Redline", 11, 50);

    for(int i=0;i<bikes.length;i++){
      System.out.println(bikes[i]);
    }
  }
}
